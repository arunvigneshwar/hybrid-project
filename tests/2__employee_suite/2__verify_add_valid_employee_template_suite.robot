*** Settings ***
Documentation  This suite file verifies alid users are able o login to dashboard
...  and connected to test case TC_OH_03
...  Test Template concepts

Resource    ../../resources/base/CommonFunctionalities.resource
Test Setup  Launch Browser and Navigate to Url
Test Teardown  Close Browser
Test Template   Verify add valid employee Template
*** Test Cases ***
TC1  admin  admin123    John    J   wick    C:${/}Automation Concepts${/}jw.jpg

*** Keywords ***
Verify add valid employee Template
    [Arguments]  ${username}  ${password}   ${firstname}    ${middlename}   ${lastname}  ${filepath}
    Input Text    name:username    ${username}
    Input Password    name:password    ${password}
    Click Element    xpath=//button[normalize-space()='Login']
    #Element Should Contain    xpath=//p[text()='Invalid credentials']    ${expected_error}
    Click Element    xpath=//span[normalize-space()='PIM']
    Click Element    xpath=//button[normalize-space()='Add']
    Input Text    name:firstName    ${firstname}
    Input Text    name:middleName    ${middlename}
    Input Text    name:lastName    ${lastname}
    Choose File    xpath=//button[@role='none']    ${filepath}