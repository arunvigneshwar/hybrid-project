*** Settings ***
Documentation  This suite file verifies alid users are able o login to dashboard
...  and connected to test case TC_OH_03
...  Test Template concepts

Resource    ../../resources/base/CommonFunctionalities.resource
Test Setup  Launch Browser and Navigate to Url
Test Teardown  Close Browser
*** Test Cases ***
TC1
    Input Text    name:username    admin
    Input Password    name:password    admin123
    Click Element    xpath://button[text()=' Login ']
    #Element Should Contain    xpath=//p[text()='Invalid credentials']    ${expected_error}
    Click Element    xpath=//span[text()='PIM']
    Click Element    xpath=//button[text()=' Add ']
    Input Text    name:firstName    John
    Input Text    name:middleName    J
    Input Text    name:lastName    wick
    Choose File    xpath=//button[@role='none']    C:${/}Automation Concepts${/}jw.jpg