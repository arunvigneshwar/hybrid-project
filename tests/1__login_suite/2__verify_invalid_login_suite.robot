*** Settings ***
Documentation  This suite file verifies alid users are able o login to dashboard
...  and connected to test case TC_OH_03
...  Test Template concepts

Resource    ../../resources/base/CommonFunctionalities.resource
Test Setup  Launch Browser and Navigate to Url
Test Teardown  Close Browser
Test Template   Verify Invalid Login Template

*** Test Cases ***
TC1  john   admin123  Invalid credentials
TC1  test   admin123  Invalid credentials
*** Keywords ***
Verify Invalid Login Template
    [Arguments]  ${username}  ${password}  ${expected_error}
    Input Text    name:username    ${username}
    Input Password    name:password    ${password}
    Click Element    xpath://button[text()=' Login ']
    Element Should Contain    xpath=//p[text()='Invalid credentials']    ${expected_error}
